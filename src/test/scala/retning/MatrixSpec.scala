package retning.test

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers
import retning._

class MatrixSpec extends UnitSpec {

	def allMatrices[S, M <: Matrix[S,M]](Mat : MatrixObject[S,M])(implicit i2s : Int => S) {

		val Zero = Mat.Zero
		val Id = Mat.Identity

		describe("==") {
			it("returns true for identical matrices") {
				(Id == Id) should be(true)
			}

			it("returns false for different matrices with same values") {
				val other = Id*2
				(Id*4 == other*2) should be(true)
			}

			it("returns false for different matrices with different values") {
				(Id == Id*13) should be(false)
			}
		}


		describe("indexing") {
			it("returns the value in the indexed cell") {
				val m = Id
				m(0,0) should be === 1
				m(m.rowCount-1,1) should be === 0
			}

			it("throws an exception when index out of range") {
				val m = Id
				intercept[IndexOutOfBoundsException]{ m(-1,0) }
				intercept[IndexOutOfBoundsException]{ m(0,-1) }
				intercept[IndexOutOfBoundsException]{ m(m.rowCount-1,m.columnCount+4) }
			}
		}

		describe("*") {

			it("identity matrix multiplied with itself returns identity") {
				(Id * Id) should be ===(Id)
			}

			it("Identity times any matrix A returns A") {
				val a = Id.map(_ => -3)
				(a*Id) should be === a
			}
			
			it("multiplying a matrix with its inverse returns the identity") (pending)
		}

		describe("+") {

			it("adding a matrix to itself is the same as multiplying by 2") {
				(Id+Id) should be === Id*2
			}
		}

		describe("map(Int,Int)") {
			it("visits each cell") {
				val m = Id
				m.map((i,j) => 1) should be === Mat(1)
			}

			it("does not change original matrix") {
				val m = Zero
				m.map((i,j) => 5)
				m(0,0) should be === 0
			}
		}

		describe("map(Scalar)") {
			it("visits each cell") {
				val m = Id
				m.map(value => 1) should be === Mat(1)
			}

			it("does not change the original matrix") {
				val m = Mat.Zero
				m.map(value => 5)
				m should be === Mat(0)
			}
		}
	}


	describe("Matrix3x3f") {

		type Mat = Matrix3x3f
		val Mat = Matrix3x3f
		val Id = Mat.Identity

		it should behave like allMatrices[Float,Matrix3x3f](Mat)

		describe("constructor(Float)") {
			it("sets all entries to the passed value") {
				Mat(0) should be ===Mat(0,0,0,0,0,0,0,0,0)
			}
		}

	}


	describe("Matrix4x4f") {

		type Mat = Matrix4x4f
		val Mat = Matrix4x4f

		val Id = Mat.Identity
		val Zero = Mat.Zero

		describe("constructor(Float)") {
			it("sets all entries to the passed value") {
				Mat(0) should be ===Mat(0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0)
			}
		}

		it should behave like allMatrices[Float,Matrix4x4f](Mat)
		
	}

}
