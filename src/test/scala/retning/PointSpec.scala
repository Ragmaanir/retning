package retning.test

import org.scalatest.{FunSpec, Spec}
import org.scalatest.matchers.ShouldMatchers

import retning.Points.{Point3f,Point3i}
import retning.Vectors._

class PointSpec extends UnitSpec {
	
	describe("Point3f") {
		describe("-") {
			it("returns distance vector") {
				(Point3f(0,0,0) - Point3f(1,1,1)) should  be ===(Vector3f(-1))
			}
		}
		
		describe("+") {
			it("returns new point offsetted by distance") {
				//(Point3f(1, 3.0f, 0) + Vector3f(1, -3, -1)) should be ===(Vector3f(2, 0.0f, -1))
				val dist = Vector3f(-1, -0.5, 2)
				val pt = Point3f(1, 2, 3)
				((pt + dist) - pt).isCloseTo(dist) should be===(true)
			}
		}
		
		describe("<<=") {
			it("returns true for equal point") {
				(Point3f(1, -2, 5) <<= Point3f(1, -2, 5)) should be===(true)
			}
			it("returns true for definitely smaller point") {
				(Point3f(1, -2, 5) <<= Point3f(1, -1, 22)) should be===(true)
			}
			it("returns false for point that is not definitely smaller") {
				(Point3f(1, -2, 5) <<= Point3f(1, -3, 22)) should be===(false)
			}
		}
	}
	
}
