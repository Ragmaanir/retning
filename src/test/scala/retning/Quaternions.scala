package retning.test

import retning.Quaternions._
import retning.Vectors.Vector3f
import retning.Angle._

import retning.test.matchers.NumberMatchers

class Quaternions extends UnitSpec with NumberMatchers  {

	describe("Quaternions") {

		def rand = math.random.toFloat

		def anyQuat = Quatf(rand,rand,rand,rand).normalized

		it("a' * b' == (b*a)'") {
			val a = anyQuat
			val b = anyQuat
			
			(a.conjugate * b.conjugate).closeTo((b*a).conjugate,1.0E-5f) should equal(true)
		}

		it("The magnitude of q*q' should be the same as the magnitude of q") {
			val q = anyQuat
			(q * q.conjugate).magnitude should be(closeTo(q.magnitude,1.0E-5f))
		}

		it("The magnitude of q*q' for a normalized quaternion should be 1") {
			val q = anyQuat
			(q * q.conjugate).magnitude should be(closeTo(1,1.0E-5f))
		}

    describe("#normalized") {
      it("equals itself when already normalized") {
        val u = Quatf(0,0,0,1)

        u.normalized should equal(u)
      }

      it("has the magnitude 1") {
        anyQuat.normalized.magnitude should be(closeTo(1))
      }
    }

    describe("#+") {
      it("adds component wise") {
        Quatf(-1,1,1,1) + Quatf(1,2,3,4) should equal(Quatf(0,3,4,5))
      }
    }

    describe("#*") {
      it("computes the correct value") {
        Quatf(1,0,1,0) * Quatf(1,0,1,0) should equal(Quatf(0,0,2,0))
        Quatf(1,0,0,1) * Quatf(1,2,3,4) should equal(Quatf(-3,-1,5,5))
        Quatf(0,0,0,0) * Quatf(1,2,3,4) should equal(Quatf(0,0,0,0))
      }

      it("is not commutative") {
        val q1 = Quatf(1,2,3,4)
        val q2 = Quatf(4,0,2,1)

        q1*q2 shouldNot equal(q2*q1)
      }
    }

    describe("#fromAxisAngle") {
      it("computes correct value") {
        Quatf.fromAxisAngle(Vector3f(1,0,0), 90.deg).closeTo(Quatf(0.70710677f, 0.70710677f, 0.0f, 0.0f)) should equal(true)
      }
    }

	}
}
