package retning.test


import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers
import retning.Points.{Point3f => Point3r}
import retning.Volumes.{Sphere, Cuboid}
import retning.Vectors._


class VolumesSpec extends UnitSpec {

	/*
	describe("Volumetric") {
		describe("intersectsVolume") {
			it("throws") {
				
				intercept()
			}
		}
	}
	*/

	describe("Cuboid") {

		def cuboid = Cuboid(Point3r(0,0,0),Point3r(2,2,2))

		describe("midPoint") {
			it("is between min and max") {
				val min = Point3r(0,1,-1)
				val max = Point3r(1,10,-0.5f)
				val cuboid = Cuboid(min,max)
				//(cuboid.midPoint - min) should be===(cuboid.size/2.0)
				(cuboid.midPoint - min).isCloseTo(cuboid.size/2.0) should be===(true)
			}
		}

		describe("intersectsVolume") {

			describe("returns true for") {
				val result = true

				it("an intersecting sphere") {
					cuboid.intersectsVolume(Sphere(cuboid.maxPoint,1)) should be ===(result)
				}

				it("an fully contained sphere") {
					cuboid.intersectsVolume(Sphere(cuboid.midPoint,0.2f)) should be ===(result)
				}
				it("an overlapping contained sphere") {
					cuboid.intersectsVolume(Sphere(cuboid.midPoint,1.1f)) should be ===(result)
				}
				it("an containing sphere") {
					cuboid.intersectsVolume(Sphere(cuboid.midPoint,10)) should be ===(result)
				}
			}

			describe("returns false for") {
				val result = false
				it("a sphere far away") {
					cuboid.intersectsVolume(Sphere(Point3r(10,10,10),1)) should be ===(result)
				}
			}
		}
		
		describe("containsPoint") {
			describe("returns true for") {
				val result = true
				it("the midPoint") {
					cuboid.containsPoint(cuboid.midPoint) should be===(result)
				}
				it("the minPoint") {
					cuboid.containsPoint(cuboid.minPoint) should be===(result)
				}
			}
			describe("returns false for") {
				val result = false
				it("a smaller point than minPoint") {
					cuboid.containsPoint(cuboid.minPoint - Vector3f(1,0,0)) should be===(result)
				}
				it("a bigger point than maxPoint") {
					cuboid.containsPoint(cuboid.maxPoint + Vector3f(1,0,0)) should be===(result)
				}
			}
		}

	}

	describe("Sphere") {

	}
}
