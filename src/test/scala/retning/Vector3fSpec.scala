package retning.test

import retning.Vectors.{Vector3f => Vec3f}

class Vector3fSpec extends UnitSpec {

	describe("Vec3f") {

		it("Zero vector should have length 0") {
			Vec3f.Zero.length should equal(0)
		}

		it("length should be square root of squared length") {
			val v = Vec3f(1,2,3)
			v.length should equal(scala.math.sqrt(v.squaredLength).toFloat)
		}

		it("A normalized vector should have length 1") {
			Vec3f(4,3,1).normalized.length should equal(1.0)
		}

		describe("scalar operations") {

			it("+ should add a scalar") {
				(Vec3f.Zero + 1) should equal(Vec3f(1))
			}

			it("- should subtract a scalar") {
				(Vec3f(1) - 1) should equal(Vec3f.Zero)
			}

			it("* should multiply with scalar") {
				val v = Vec3f(1,2,3)*2
				v.isCloseTo(Vec3f(2,4,6)) should equal(true)
			}

			it("/ should divide by scalar") {
				val v = Vec3f(2,4,6)/2

				v.isCloseTo(Vec3f(1,2,3)) should equal(true)
			}
		}

		it("abs should strips signs") {
			Vec3f(-4,0.5,-0.123).abs.isCloseTo(Vec3f(4,0.5,0.123)) should equal(true)
		}

		it("should be close to other vector") {
			(Vec3f(1,-5,64)*2).isCloseTo(Vec3f(2,-10,128)) should equal(true)
		}

		it("should compute cross product") {
			Vec3f(1,0,0).cross(Vec3f(0,1,0)) should equal(Vec3f(0,0,1))
			Vec3f(0,1,0).cross(Vec3f(1,0,0)) should equal(Vec3f(0,0,-1))
			Vec3f(5,0,0).cross(Vec3f(0,3,0)) should equal(Vec3f(0,0,15))
		}

		it("dot product of two orthogonal vectors should be zero") {
			Vec3f(4,0,0).dot(Vec3f(0,-55,0)) should equal(0)
		}

		it("dot product of a vector with itself should equal its square length") {
			val v = Vec3f(3,6,-4)
			v.dot(v) should equal(v.squaredLength)
		}

		it("dot product should be distributive") {
			val a = Vec3f(4,2,-4)
			val b = Vec3f(0.123,-6,0)
			val c = Vec3f(0,0,22)

			a.dot(b+c) should equal(a.dot(b)+a.dot(c))
		}
	}

}
