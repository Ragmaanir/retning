package retning.test.algorithms

import retning.test.UnitSpec
import retning.Volumes
import retning.Volumes.Point3r
import retning.algorithms.IntersectionTester.Intersection
import retning.Vectors.Vector3f

/**
 * Created by ragmaanir on 4/1/14.
 */
class DefaultIntersectionTester extends UnitSpec {

  describe("intersectionWith") {
    describe("between Spheres") {
      it("returns some intersection when intersecting") {
        import retning.algorithms.DefaultIntersectionTester._

        val v1 = new Volumes.Sphere(Point3r(0,0,0), 1.0f)
        val v2 = new Volumes.Sphere(Point3r(1,0,0), 1.0f)

        v1.intersectionWith(v2) should be === Some(Intersection(Point3r(-1,0,0),Vector3f(-1,0,0)))
      }

      it("returns none when not intersecting") {
        import retning.algorithms.DefaultIntersectionTester._

        val v1 = new Volumes.Sphere(Point3r(0,0,0), 1.0f)
        val v2 = new Volumes.Sphere(Point3r(3,0,0), 1.0f)

        v1.intersectionWith(v2) should be === None
      }
    }
  }

}
