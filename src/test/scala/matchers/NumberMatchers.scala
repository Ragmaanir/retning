package retning.test.matchers

import org.scalatest.matchers._

trait NumberMatchers {

	/*
	def beCloseTo(expectedValue: Float, threshold : Float = Float.MinPositiveValue) =
		new HavePropertyMatcher[Float, Float] {
		  def apply(value : Float) =
			HavePropertyMatchResult(
			  math.abs(value - expectedValue) <= threshold,
			  "close to",
			  expectedValue,
			  value
			)
		}
	*/

	def closeTo(expectedValue: Float, threshold : Float = Float.MinPositiveValue) =
		new BeMatcher[Float] {
			def apply(value : Float) = {
				val failure = value + " was not close to "+expectedValue
				val negFailure = value + " was close to "+expectedValue
				MatchResult(
					math.abs(value - expectedValue) <= threshold,
					failure,
					negFailure,
					failure,
					negFailure
				)
			}
		}

	/*
	def closeTo[T <% ThresholdComparable](expected : T, threshold : Float = Float.MinPositiveValue) = new BeMatcher[T]{
		def apply(actual : T) = {
			val failure = actual + " was not close to "+expected
			val negFailure = actual + " was close to "+expected
			MatchResult(
				expected.closeTo(actual,threshold),
				failure,
				negFailure,
				failure,
				negFailure
			)
		}
	}
	*/

	/*
	def closeTo[T <: {def closeTo(t:T,f:Float) : Boolean}](expected : T, threshold : Float = Float.MinPositiveValue) = new BeMatcher[T]{
		def apply(actual : T) = {
			val failure = actual + " was not close to "+expected
			val negFailure = actual + " was close to "+expected
			MatchResult(
				expected.closeTo(actual,threshold),
				failure,
				negFailure,
				failure,
				negFailure
			)
		}
	}
	*/
}

object NumberMatchers extends NumberMatchers
