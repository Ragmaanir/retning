package retning.geometry

import retning.Points.{Point3f => Point}
import retning.Vectors.{Vector3f => Vector}

object Primitives {
  trait Primitive {
    def containsPoint(point : Point, tolerance : Float = Float.MinPositiveValue) : Boolean
  }

  case class Line(val origin : Point, direction0 : Vector) extends Primitive {
    val direction = direction0.normalized

    def containsPoint(point : Point, tolerance : Float = Float.MinPositiveValue) = {
      val dir = point.asVector - origin.asVector
      val lambda = dir(0) / direction(0)
      math.abs(lambda * dir(1) - direction(1)) <= tolerance && math.abs(lambda * dir(2) - direction(2)) <= tolerance
    }

    override def hashCode = direction.hashCode
    override def equals(other : Any) = other match {
      case Line(_, dir) => direction == dir.abs
      case _ => false
    }
  }

  case class Ray(val origin : Point, direction0 : Vector) extends Primitive {
    val direction = direction0.normalized

    def containsPoint(point : Point, tolerance : Float = Float.MinPositiveValue) = {
      val dir = point.asVector - origin.asVector
      val lambda = dir(0) / direction(0)
      math.abs(lambda * dir(1) - direction(1)) <= tolerance && math.abs(lambda * dir(2) - direction(2)) <= tolerance
    }

    override def hashCode = direction.hashCode
    override def equals(other : Any) = other match {
      case Ray(_, dir) => direction == dir
      case _ => false
    }
  }

  case class LineSegment(val start : Point, end : Point) extends Primitive {
    def direction = (end - start).normalized

    def containsPoint(point : Point, tolerance : Float = Float.MinPositiveValue) = {
      val dir = point.asVector - start.asVector
      /*val lambda = dir(0) / direction(0)
      math.abs(lambda * dir(1) - direction(1)) <= tolerance && math.abs(lambda * dir(2) - direction(2)) <= tolerance*/
      false // FIXME implement
    }

    override def hashCode = start.hashCode
    override def equals(other : Any) = other match {
      case l : LineSegment => start == l.start && end == l.end
      case _ => false
    }
  }

}
