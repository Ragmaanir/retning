package retning

// TODO wrap this in an object "Matrices"
sealed abstract class Matrix[Scalar,M <: Matrix[Scalar,M]] {

	def unary_+ : M
	def unary_- : M
	def abs : M

	def *(s : Scalar) : M
	def /(s : Scalar) : M //= {val inv = 1/s; this*inv }

	def +(other : M) : M
	def -(other : M) : M = this + (-other)

	def *(other : M) : M

	def apply(row : Int, column : Int) : Scalar

	// inner arrays each represent one row
	//def toArray : Array[Array[Scalar]]
	def toList : List[List[Scalar]]

	//def map(f : Scalar => Scalar) : M
	def map(f : (Int,Int) => Scalar) : M
	final def map(f : Scalar => Scalar) : M = map((i,j) => f(this(i,j)))
	def each(f : Scalar => Nothing) : Unit

	def rows : List[List[Scalar]]
	def columns : List[List[Scalar]]
	
	def row(i : Int) : List[Scalar]
	def column(i : Int) : List[Scalar]

	def rowCount : Int
	def columnCount : Int

	override def toString = {
		rows.map(_.mkString("[",",","]")).mkString("[",",\n","]")
	}
}

trait MatrixObject[Scalar, M <: Matrix[Scalar,M]] {
	val Rows : Int
	val Columns : Int

	val Identity : M
	val Zero : M

	def apply(vs :Scalar*) : M
	def apply(v :Scalar) : M
}

/**
 * Matrix3x3
 * Row x Columns
 */
sealed abstract class Matrix3x3[Scalar,M <: Matrix3x3[Scalar,M]](
		v00 : Scalar, v01 : Scalar, v02 : Scalar,
		v10 : Scalar, v11 : Scalar, v12 : Scalar,
		v20 : Scalar, v21 : Scalar, v22 : Scalar
	) extends Matrix[Scalar,M] {

	final override def apply(row : Int, column : Int) : Scalar = row match {
		case 0 => column match {
			case 0 => v00
			case 1 => v01
			case 2 => v02
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)//sys.error("invalid column: "+column)
		}
		case 1 => column match {
			case 0 => v10
			case 1 => v11
			case 2 => v12
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case 2 => column match {
			case 0 => v20
			case 1 => v21
			case 2 => v22
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case _ => throw new IndexOutOfBoundsException("invalid row: "+row)
	}

	//final override def each(f : Scalar => Nothing) = toArray.flatten.foreach(f(_))
	final override def each(f : Scalar => Nothing) = toList.flatten.foreach(f(_))
	/*
	final override def toArray : Array[Array[Scalar]] = Array[Array[Scalar]](
		Array(v00,v01,v02),
		Array(v10,v11,v12),
		Array(v20,v21,v22)
	)
	*/

	final override def rowCount : Int = 3
	final override def columnCount : Int = 3

	final override def rows = List(row(0),row(1),row(2))
	final override def columns = List(column(0),column(1),column(2))

	final override def row(i : Int) = i match {
		case 0 => List(v00,v01,v02)
		case 1 => List(v10,v11,v12)
		case 2 => List(v20,v21,v22)
	}

	final override def column(i : Int) = i match {
		case 0 => List(v00,v10,v20)
		case 1 => List(v01,v11,v21)
		case 2 => List(v02,v12,v22)
	}

	final override def toList : List[List[Scalar]] = List[List[Scalar]](
		List(v00,v01,v02),
		List(v10,v11,v12),
		List(v20,v21,v22)
	)
}

object Matrix3x3f extends MatrixObject[Float, Matrix3x3f] {

	val Mat = Matrix3x3f
	type Mat = Matrix3x3f

	lazy val Rows = 3
	lazy val Columns = 3

	lazy val Identity = Mat(
		1,0,0,
		0,1,0,
		0,0,1
	)

	lazy val Zero = Mat(0)

	//def apply(v : Float) : Mat = Mat(0.to(Rows*Columns-1).map(x => v) : _*)
	def apply(v : Float) : Mat = Identity.map(_ => v)

	def apply(vs : Float*) = {
		require(vs.length == Rows*Columns)
		new Matrix3x3f(
			vs(0),vs(1),vs(2),
			vs(3),vs(4),vs(5),
			vs(6),vs(7),vs(8)
		)
	}
}

final class Matrix3x3f(
		v00 : Float, v01 : Float, v02 : Float,
		v10 : Float, v11 : Float, v12 : Float,
		v20 : Float, v21 : Float, v22 : Float
	) extends Matrix3x3[Float,Matrix3x3f](
		v00, v01, v02,
		v10, v11, v12,
		v20, v21, v22
	) {

	type Mat = Matrix3x3f
	type Scalar = Float
	
	final override def unary_+ = this
	final override def unary_- = this * -1

	final override def abs = map(scala.math.abs(_))

	final override def *(s : Scalar) : Mat = map(_*s)
	final override def /(s : Scalar) : Mat = {val inv = 1/s; this*inv }

	final override def +(other : Mat) : Mat = map((row,col) => this(row,col) + other(row,col))

	final override def *(m : Matrix3x3f) : Matrix3x3f = Matrix3x3f(
		(for(i <- 0 to rowCount-1; j <- 0 to columnCount-1)
			yield { row(i).zip(m.column(j)).map(e => e._1*e._2).sum }) : _*
	)

	/*
	final override def *(m : Matrix3x3f) : Matrix3x3f = Matrix3x3f(
		/*v00*m.v00 + v01*m.v10 + v02*m.v20,
		v10*m.v01 + v11*m.v11 + v12*m.v21,
		v20*m.v02*/
		//(0 to 8).map{}
		
		(for(i <- 0 to 2; j <- 0 to 2)
			yield { row(i).zip(m.column(j)).map(e => e._1*e._2).sum }) : _*


		/*
		v00*m(0,0) + v01*m(1,0) + v02*m(2,0),
		v10*m(0,1) + v11*m(1,1) + v12*m(2,1),
		v20*m(0,2) + v21*m(1,2) + v22*m(2,2)
		*/
	)
	*/
	
	final override def map(f : (Int,Int) => Scalar) = Matrix3x3f(
		f(0,0),f(0,1),f(0,2),
		f(1,0),f(1,1),f(1,2),
		f(2,0),f(2,1),f(2,2)
	)

	/*
	final override def map(f : Scalar => Scalar) = Matrix3x3f(
		f(v00),f(v01),f(v02),
		f(v10),f(v11),f(v12),
		f(v20),f(v21),f(v22)
	)
	*/

	final override def equals(other : Any) = {
		other match {
			//case m : Matrix3x3f => other.map((row,col) => this(row,col) == other(row,col))
			case m : Mat => m.toList == toList
			case _ => false
		}
	}
}

/**
 * Matrix4x4
 * Row x Columns
 */
sealed abstract class Matrix4x4[Scalar,M <: Matrix4x4[Scalar,M]](
		v00 : Scalar, v01 : Scalar, v02 : Scalar, v03 : Scalar,
		v10 : Scalar, v11 : Scalar, v12 : Scalar, v13 : Scalar,
		v20 : Scalar, v21 : Scalar, v22 : Scalar, v23 : Scalar,
		v30 : Scalar, v31 : Scalar, v32 : Scalar, v33 : Scalar
	) extends Matrix[Scalar,M] {

	final override def apply(row : Int, column : Int) : Scalar = row match {
		case 0 => column match {
			case 0 => v00
			case 1 => v01
			case 2 => v02
			case 3 => v03
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case 1 => column match {
			case 0 => v10
			case 1 => v11
			case 2 => v12
			case 3 => v13
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case 2 => column match {
			case 0 => v20
			case 1 => v21
			case 2 => v22
			case 3 => v23
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case 3 => column match {
			case 0 => v30
			case 1 => v31
			case 2 => v32
			case 3 => v33
			case _ => throw new IndexOutOfBoundsException("invalid column: "+column)
		}
		case _ => throw new IndexOutOfBoundsException("invalid row: "+row)
	}

	final override def each(f : Scalar => Nothing) = toList.flatten.foreach(f(_))

	final override def rowCount : Int = 4
	final override def columnCount : Int = 4

	final override def rows = List(row(0),row(1),row(2),row(3))
	final override def columns = List(column(0),column(1),column(2),column(3))

	final override def row(i : Int) = i match {
		case 0 => List(v00,v01,v02,v03)
		case 1 => List(v10,v11,v12,v13)
		case 2 => List(v20,v21,v22,v23)
		case 3 => List(v30,v31,v32,v33)
	}

	final override def column(i : Int) = i match {
		case 0 => List(v00,v10,v20,v30)
		case 1 => List(v01,v11,v21,v31)
		case 2 => List(v02,v12,v22,v32)
		case 3 => List(v03,v13,v23,v33)
	}

	final override def toList : List[List[Scalar]] = List[List[Scalar]](
		List(v00,v01,v02,v03),
		List(v10,v11,v12,v13),
		List(v20,v21,v22,v23),
		List(v30,v31,v32,v33)
	)
}

object Matrix4x4f extends MatrixObject[Float, Matrix4x4f] {

	val Mat = Matrix4x4f
	type Mat = Matrix4x4f

	lazy val Identity = Mat(
		1,0,0,0,
		0,1,0,0,
		0,0,1,0,
		0,0,0,1
	)

	lazy val Zero = Mat(0)

	lazy val Rows = 4
	lazy val Columns = 4

	//def apply(v : Float) : Mat = Mat(0.to(Rows*Columns-1).map(x => v) : _*)
	def apply(v : Float) : Mat = Identity.map(_ => v)

	def apply(vs : Float*) = {
		require(vs.length == Rows*Columns)
		new Matrix4x4f(
			vs(0),vs(1),vs(2),vs(3),
			vs(4),vs(5),vs(6),vs(7),
			vs(8),vs(9),vs(10),vs(11),
			vs(12),vs(13),vs(14),vs(15)
		)
	}
}

final class Matrix4x4f(
		v00 : Float, v01 : Float, v02 : Float, v03 : Float,
		v10 : Float, v11 : Float, v12 : Float, v13 : Float,
		v20 : Float, v21 : Float, v22 : Float, v23 : Float,
		v30 : Float, v31 : Float, v32 : Float, v33 : Float
	) extends Matrix4x4[Float,Matrix4x4f](
		v00, v01, v02, v03,
		v10, v11, v12, v13,
		v20, v21, v22, v23,
		v30, v31, v32, v33
	) {

	type Scalar = Float
	type Mat = Matrix4x4f

	final override def unary_+ = this
	final override def unary_- = this * -1

	final override def abs = map(scala.math.abs(_))

	final override def *(s : Scalar) : Mat = map(_*s)
	final override def /(s : Scalar) : Mat = { val inv = 1/s; this*inv }

	final override def +(other : Mat) : Mat = map((row,col) => this(row,col) + other(row,col))

	final override def *(m : Matrix4x4f) : Matrix4x4f = Matrix4x4f(
		(for(i <- 0 to rowCount-1; j <- 0 to columnCount-1)
			yield { row(i).zip(m.column(j)).map(e => e._1*e._2).sum }) : _*
	)

	final override def map(f : (Int,Int) => Scalar) = Matrix4x4f(
		(for(i <- 0 to rowCount-1; j <- 0 to columnCount-1)
			yield { f(i,j) }) : _*
	)

	/*Matrix4x4f(
		f(0,0),f(0,1),f(0,2),f(0,3),
		f(1,0),f(1,1),f(1,2),f(1,3),
		f(2,0),f(2,1),f(2,2),f(2,3),
		f(3,0),f(3,1),f(3,2),f(3,3)
	)*/

	//final override def map(f : Scalar => Scalar) = map((i,j) => f(this(i,j)))
	/*Matrix4x4f(
		f(v00),f(v01),f(v02),f(v03),
		f(v10),f(v11),f(v12),f(v13),
		f(v20),f(v21),f(v22),f(v23),
		f(v30),f(v31),f(v32),f(v33)
	)*/

	final override def equals(other : Any) = {
		other match {
			//case m : Matrix3x3f => other.map((row,col) => this(row,col) == other(row,col))
			case m : Mat => m.toList == toList
			case _ => false
		}
	}
}
