package retning

object Quaternions {

	import Vectors._

	/*
	type GenericScalar = {
		def +(o : Scalar) : Scalar
		def *(o : Scalar) : Scalar
		def /(o : Scalar) : Scalar
	}

	trait ScalarT {
		def +(o : ScalarT) : ScalarT
		def *(o : ScalarT) : ScalarT
		def /(o : ScalarT) : ScalarT
	}
	*/

	/*
	trait Scalar {
		type S <: Scalar
		type N

		def +(o : N) : N
		def *(o : N) : N
		def /(o : N) : N

		def +(o : S) : N
		def *(o : S) : N
		def /(o : S) : N
	}

	class ScalarFloat(val v : Float) extends Scalar {
		type S = ScalarFloat
		type N = Float

		def +(o : N) : N = v+o
		def *(o : N) : N = v*o
		def /(o : N) : N = v/o

		def +(o : S) : N = v + o.v
		def *(o : S) : N = v * o.v
		def /(o : S) : N = v / o.v
	}

	class ScalarDouble(val v : Double) extends Scalar {
		type S = ScalarFloat
		type N = Double

		def +(o : N) : N = v+o
		def *(o : N) : N = v*o
		def /(o : N) : N = v/o

		def +(o : S) : N = v + o.v
		def *(o : S) : N = v * o.v
		def /(o : S) : N = v / o.v
	}
	*/


	sealed abstract class Quaternion[Scalar] {

		/*
		implicit def f2s(f : Float) = new ScalarFloat(f)
		implicit def d2s(d : Double) = new ScalarDouble(d)

		implicit def s2f(f : ScalarFloat) = f.v
		implicit def s2d(d : ScalarDouble) = d.v
		*/

		type Quat <: Quaternion[Scalar]
		type Vec <: Vector[Scalar]

		import scala.math

		/*
		val x : Scalar
		val y : Scalar
		val z : Scalar
		val w : Scalar
		*/

		protected def create(qw : Scalar, qx : Scalar, qy : Scalar, qz : Scalar) : Quat

		def +(q : Quat) : Quat
		def -(q : Quat) : Quat
		def unary_-() : Quat

		def *(q :Quat) : Quat

		def *(s : Scalar) : Quat

		def transform(vec : Vec) : Vec

		def magnitude : Scalar
		def normalized : Quat
		def conjugate : Quat
		//def inverse : Quat

		def forall(p : Scalar=>Boolean) : Boolean

		/*
		def +(q : Quat) : Quat = create(
			x + q.x,
			y + q.y,
			z + q.z,
			w + q.w
		)

		def *(q :Quat) : Quat = create(
			x * q.w + y * q.z - z * q.y + w * q.x,
			-x * q.z + y * q.w + z * q.x + w * q.y,
			x * q.y - y * q.x + z * q.w + w * q.z,
			-x * q.x - y * q.y - z * q.z + w * q.w
		)

		def *(s : Scalar) : Quat = create(x*s,y*s,z*s,w)

		def transform(vec : Vector[Scalar]) : Vector[Scalar]

		def length = math.sqrt(x*x + y*y + z*z + w*w)
		def normalized : Quat = {
			val l = length
			create(x/l,y/l,z/l,w/l)
		}
		*/
	}

	object Quatf {

    val Zero = Quatf(0,0,0,0)

		def apply(qw : Float, qx : Float, qy : Float, qz : Float) = new Quatf(qw,qx,qy,qz)

		def fromAxisAngle(axis : Vector3f, angle : Angle) : Quatf = {
			val a = angle.inRadians/2
			import scala.math._
			implicit def d2f(d : Double) = d.toFloat
			Quatf(cos(a), axis.x * sin(a), axis.y * sin(a), axis.z * sin(a))
		}
	}

	final class Quatf private(val w : Float, val x : Float, val y : Float, val z : Float) extends Quaternion[Float] {
		type Quat = Quatf
		type Scalar = Float
		type Vec = Vector3[Scalar]

		override protected def create(qw : Scalar, qx : Scalar, qy : Scalar, qz : Scalar) : Quat = Quatf(qw,qx,qy,qz)
		protected def vec(x : Scalar, y : Scalar, z : Scalar) = Vector3f(x,y,z)

		def unary_-() : Quat = create(-w,-x,-y,-z)

		def +(q : Quat) : Quat = create(
      w + q.w,
			x + q.x,
			y + q.y,
			z + q.z
		)

		def -(q : Quat) : Quat = this + (-q)

    def *(q :Quat) : Quat = create(
      -x * q.x  -  y * q.y  -  z * q.z  +  w * q.w,
       x * q.w  +  y * q.z  -  z * q.y  +  w * q.x,
      -x * q.z  +  y * q.w  +  z * q.x  +  w * q.y,
       x * q.y  -  y * q.x  +  z * q.w  +  w * q.z
    )

		def *(s : Scalar) : Quat = create(w,x*s,y*s,z*s)

		//def transform(vec : Vec) : Vec = this * create(vec.x,vec.y,vec.z,0) * conjugate

		def transform(v : Vec) : Vec = vec(
			w*w*v.x   + 2*y*w*v.z - 2*z*w*v.y + x*x*v.x   + 2*y*x*v.y + 2*z*x*v.z - z*z*v.x   - y*y*v.x,
			2*x*y*v.x + y*y*v.y   + 2*z*y*v.z + 2*w*z*v.x - z*z*v.y   + w*w*v.y   - 2*x*w*v.z - x*x*v.y,
			2*x*z*v.x + 2*y*z*v.y + z*z*v.z   - 2*w*y*v.x - y*y*v.z   + 2*w*x*v.y - x*x*v.z   + w*w*v.z
		)

		def magnitude = math.sqrt(w*w + x*x + y*y + z*z).toFloat
		def normalized : Quat = {
			val l = magnitude
			create(w/l,x/l,y/l,z/l)
		}

		def conjugate : Quat = create(w,-x,-y,-z)
		//def inverse : Quat

		def forall(p : Scalar=>Boolean) : Boolean = values.forall(p)

		def values = Seq(w,x,y,z)

		def closeTo(other : Quatf, threshold : Scalar = Float.MinPositiveValue) = {
			(this-other).forall(math.abs(_) <= threshold)
		}

		//override def hashCode = (41 + 41*(x + 41*(y + 41*(z + 41*w)))).toInt
		//override def hashCode = values.foldLeft((acc,v) => acc + v.hashCode)
		override def hashCode = x.hashCode + y.hashCode + z.hashCode + w.hashCode
		override def equals(other :Any) = other match {
			case q : Quatf => w == q.w && x == q.x && y == q.y && z == q.z
			case _ => sys.error("invalid comparison: '"+toString+"' with '"+other.toString+"'")
		}

		override def toString = "Quatf("+values.mkString(",")+")"
	}

}
