package retning.algorithms

import retning.Volumes._
import retning.Points.{Point3f => Point}
import retning.Vectors.{Vector3f => Vector}
import retning.algorithms.IntersectionTester.Intersection

object DefaultIntersectionTester extends IntersectionTester {

  sealed class IntersectableVolume(v : Volume) {
    def intersectionWith(other : Volume) : Option[Intersection] = DefaultIntersectionTester.intersectionBetween(v, other)
  }

  implicit def extendVolume(v : Volume) = new IntersectableVolume(v)

  def intersectionBetween(a : Volume, b : Volume/*, reversed : Boolean = false*/) : Option[Intersection] = {
    (a,b) match {
      case (sphericalA : Spherical, sphericalB : Spherical) => intersectionBetweenSphericals(sphericalA,sphericalB)
      case (cuboidalA : Cuboidal, cuboidalB : Cuboidal) => intersectionBetweenCuboidals(cuboidalA, cuboidalB)
    }

    /*a match {
      case spherical : Spherical => intersectionWith(spherical,b)
      case cuboidal : Cuboidal => intersectionWith(cuboidal,b)
      //case _ => intersectionBetween(b,a,true)
      case _ => reversed match {
        case false => intersectionBetween(b,a,true)
        case true => sys.error("could not match type of volumes for intesection")
      }
    }*/
  }

  def intersectionBetweenSphericals(self : Spherical, other : Spherical) : Option[Intersection] = {
    import self._
    val dist = centerPoint - other.centerPoint
    val penetrationDepth = math.max(radius + other.radius - dist.length,0)
    //dist.length <= radius + spherical.radius
    penetrationDepth match {
      case 0 => None
      case _ => {
        val normal = dist.normalized
        Some(new Intersection(centerPoint + (normal * penetrationDepth),normal))
      }
    }
  }

  def intersectionBetweenCuboidals(self : Cuboidal, other : Cuboidal) : Option[Intersection] = {
    import self._
    val intersects = 0.to(2).forall(ci => {
      maxPoint(ci) > 1 // FIXME implement
    })

    intersects match {
      case true => Some(new Intersection(Point(0,0,0),Vector.Zero)) // FIXME implement
      case false => None
    }
  }

  /*def intersectionWithSpherical(self : Spherical, other : Volume) : Option[Intersection] = {
    require(self!=null)
    require(other!=null)
    import self._

    other match {
      case spherical : Spherical => {
        val dist = centerPoint - spherical.centerPoint
        dist.length <= radius + spherical.radius
      }
      case cuboidal : Cuboidal => {
        var dist = 0.0d

        for(ci <- 0.to(2)) {
          if(centerPoint(ci) < cuboidal.minPoint(ci)) {
            dist += math.pow(centerPoint(ci) - cuboidal.minPoint(ci),2)
          } else if(centerPoint(ci) > cuboidal.maxPoint(ci)) {
            dist += math.pow(centerPoint(ci) - cuboidal.maxPoint(ci),2)
          }
        }

        dist < radius*radius
      }
      /*case _ => reversed match {
         case true => sys.error("intersection test between "+this.getClass+" and "+other.getClass+" is not implemented")
         case false => other.intersectsVolume(this,true)
       }*/
    }
  }*/
}
