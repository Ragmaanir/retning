package retning.algorithms

import retning.Volumes.Volume
import retning.Points.{Point3f => Point}
import retning.Vectors.{Vector3f => Vector}
import retning.algorithms.IntersectionTester.Intersection

object IntersectionTester {
  case class Intersection(val intersectionPoint : Point, normal : Vector)

  /*implicit def extendVolume(v : Volume) = new {
    def intersectionBetween(a : Volume, b : Volume) : Option[Intersection] = Inter
  }*/
}

trait IntersectionTester {
  def intersectionBetween(a : Volume, b : Volume) : Option[Intersection]
}
