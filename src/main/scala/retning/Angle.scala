package retning

import scala.math

object Angle {
	implicit def fromInt(i : Int) = new AngleExtended(i)
	implicit def fromDouble(d : Double) = new AngleExtended(d)
	implicit def fromFloat(f : Float) = new AngleExtended(f)
	
	sealed class AngleExtended(value : Double) {
		def deg = new Angle(value,'degrees)
		def rad = new Angle(value,'radians)
	}
}

class Angle(value0 : Double, unit : Symbol) {
	import retning.Angle._

	val value = unit match {
		case 'degrees => math.toRadians(value0)
		case 'radians => value0
		case _ => sys.error("invalid unit: "+unit)
	}

	def inDegrees = math.toDegrees(value)
	def inRadians = value

	def <=>(other : Angle) = if(inDegrees < other.inDegrees) -1
							else if(inDegrees > other.inDegrees) 1
							else 0

	def <(other : Angle) = (this <=> other) == -1
	def >(other : Angle) = (this <=> other) == 1
	override def equals(other : Any) = other match {
		case a : Angle => this <=> a == 0
		case _ => false
	}

	def +(other : Angle) = (value + other.value).rad
	def -(other : Angle) = (value - other.value).rad

	override def toString = "Angle("+inDegrees+"°)"
}
