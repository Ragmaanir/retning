package retning

import Dimensions.Dimension3
import Vectors._

object Points {

	/*
	sealed trait GenericPoint[Scalar] {

		type Point <: GenericPoint[Scalar]

		def componentCount : Int
		def coordinates : Seq[Scalar]
		def coordinateIndices : Seq[Int]

		def apply(i : Int) : Scalar

		def -(other : Point) : Vector[Scalar]
		//def distanceTo(other : Point) : Scalar
		def distanceTo(other : Point) : Scalar = (this-other).length

		/** Returns if for all components c_i: c_i >= other.c_i */
		final def >>=(other : Point) : Boolean = coordinateIndices.forall(i => this(i) >= other(i))

		/** Returns if for all components c_i: c_i <= other.c_i */
		final def <<=(other : Point) : Boolean = coordinateIndices.forall(i => this(i) >= other(i))
	}

	abstract class Point3[Scalar] extends GenericPoint[Scalar] {
		def componentCount = 3

		def x : Scalar
		def y : Scalar
		def z : Scalar

		final def coordinates : Seq[Scalar] = Seq(x,y,z)

		final def apply(i : Int) : Scalar = i match {
			case 0 => x
			case 1 => y
			case 2 => z
			case _ => sys.error("invalid index: "+i)
		}

		override def -(other : Point) : Vector3[Scalar] = sys.error("override")
	}

	case class Point3f(val x : Float, val y : Float, val z : Float) extends Point3[Float] {
		type Scalar = Float
		type Point = Point3f

		def -(other : Point) : Vector3f = Vector3f(x-other.x, y-other.y, z-other.z)

		final override def hashCode : Int = (x+41*y+41*41*z).toInt
		final override def equals(other : Any) = other match {
			case Point3f(ox,oy,oz) => x == ox && y == oy && z == oz
			//case Point3d(ox,oy,oz) =>
			case _ => false
		}
	}
	*/

	/*
	case class Point3d(val x : Double, val y : Double, val z : Double) extends Point3[Double] {
		type Scalar = Double
		type Point = Point3d

		def -(other : Point) : Vector3d = Vector3d(x-other.x, y-other.y, z-other.z)

		final override def hashCode : Int = (x+41*y+41*41*z).toInt
		final override def equals(other : Any) = other match {
			case Point3d(ox,oy,oz) => x == ox && y == oy && z == oz
			//case Point3f(ox,oy,oz) =>
			case _ => false
		}
	}
	*/


	sealed trait GenericPoint[Scalar] {

		type Point <: GenericPoint[Scalar]
		type Vec <: Vector[Scalar]

		def componentCount : Int
		def coordinates : Seq[Scalar]
		def coordinateIndices = 0 to(componentCount-1)

		def apply(i : Int) : Scalar

		def -(other : Point) : Vec
		def +(distance : Vec) : Point
		def -(distance : Vec) : Point
		//final def -(distance : Vec) : Point = this + (-distance)
		//def distanceTo(other : Point) : Scalar
		final def distanceTo(other : Point) : Scalar = (this-other).length

		//def equalsPoint(other : Point) : Boolean = x == other.x && y == other.y && z == other.z
		//def equalsPoint(other : Point) : Boolean = coordinateIndices.forall(i => this(i) == other(i))

		/*
		sealed trait ComparisonResult
		object ComparisonResult {
			case object Less extends ComparisonResult
			case object Equal extends ComparisonResult
			case object Greater extends ComparisonResult
		}
		
		def <<==>>(other : Point) : ComparisonResult
		def <<(other : Point) : Boolean = (this <<==>> other) == ComparisonResult.Less
		def >>(other : Point) : Boolean = (this <<==>> other) == ComparisonResult.Greater
		*/

		

		protected def compareAll(other : Point, comparison : (Scalar,Scalar) => Boolean) : Boolean = {
			coordinateIndices.forall(i => comparison(this(i),other(i)))
		}
		/** Returns if for all components c_i: c_i > other.c_i */
		def <<(other : Point) : Boolean
		def <<=(other : Point) : Boolean
		//def <<=(other : Point) : Boolean = (this << other) || (this == other)

		/** Returns if for all components c_i: c_i < other.c_i */
		def >>(other : Point) : Boolean
		def >>=(other : Point) : Boolean
		//def >>=(other : Point) : Boolean = (this >> other) || (this == other)

		//def asVector[V <: Vector[_]] : V
	}

	abstract class PointF extends GenericPoint[Float] {
		type Scalar = Float

		final override def hashCode : Int = coordinateIndices.map(i => math.pow(41,i)*this(i)).sum.toInt

		//final def >>(other : Point) : Boolean = coordinateIndices.forall(i => this(i) > other(i))
		//final def <<(other : Point) : Boolean = coordinateIndices.forall(i => this(i) < other(i))
		final def <<(other : Point) : Boolean = compareAll(other,_ < _)
		final def <<=(other : Point) : Boolean = compareAll(other,_ <= _)
		final def >>(other : Point) : Boolean = compareAll(other,_ > _)
		final def >>=(other : Point) : Boolean = compareAll(other,_ >= _)


		def asPoint3i : Point3i
    def asVector : Vector3f
	}

	abstract class PointD extends GenericPoint[Double] {
		type Scalar = Double

		final override def hashCode : Int = coordinateIndices.map(i => math.pow(41,i)*this(i)).sum.toInt

		//final def >>(other : Point) : Boolean = coordinateIndices.forall(i => this(i) > other(i))
		//final def <<(other : Point) : Boolean = coordinateIndices.forall(i => this(i) < other(i))
		final def <<(other : Point) : Boolean = compareAll(other,_ < _)
		final def <<=(other : Point) : Boolean = compareAll(other,_ <= _)
		final def >>(other : Point) : Boolean = compareAll(other,_ > _)
		final def >>=(other : Point) : Boolean = compareAll(other,_ >= _)

		def asPoint3i : Point3i
    def asVector : Vector3d
	}

	abstract class PointI extends GenericPoint[Int] {
		type Scalar = Int

		final override def hashCode : Int = coordinateIndices.map(i => math.pow(41,i)*this(i)).sum.toInt

		//final def >>(other : Point) : Boolean = coordinateIndices.forall(i => this(i) > other(i))
		//final def <<(other : Point) : Boolean = coordinateIndices.forall(i => this(i) < other(i))
		final def <<(other : Point) : Boolean = compareAll(other,_ < _)
		final def <<=(other : Point) : Boolean = compareAll(other,_ <= _)
		final def >>(other : Point) : Boolean = compareAll(other,_ > _)
		final def >>=(other : Point) : Boolean = compareAll(other,_ >= _)

    def asPoint3i : Point3i
    def asVector : Vector3i
	}

	trait Coordinates3[Scalar] extends GenericPoint[Scalar] {
		type Point <: Coordinates3[Scalar]

		final def componentCount = 3

		def x : Scalar
		def y : Scalar
		def z : Scalar

		final def coordinates : Seq[Scalar] = Seq(x,y,z)

		final def apply(i : Int) : Scalar = i match {
			case 0 => x
			case 1 => y
			case 2 => z
			case _ => sys.error("invalid index: "+i)
		}

		//override def -(other : Point) : Vector3[Scalar] = sys.error("override")
	}

	/*
	object Point3f {
		//def apply(x : Float*) : Point3f = Point3f(coords(0),coords(1),coords(2))
		def apply(x : Double, y : Double, z : Double) : Point3f = Point3f(x.toFloat,y.toFloat)
		//def apply(coords : Int*) : Point3f = Point3f(coords(0),coords(1),coords(2))
	}
	*/

	case class Point3f(val x : Float, val y : Float, val z : Float) extends PointF with Coordinates3[Float] {
		type Point = Point3f
		type Vec = Vector3f

		final override def -(other : Point) : Vec = Vector3f(x-other.x, y-other.y, z-other.z)
		final override def +(distance : Vec) : Point = Point3f(x+distance.x, y+distance.y, z+distance.z)
		final override def -(distance : Vec) : Point = this + (-distance)
		//final override def -(distance : Vec) : Point = Point3f(x-distance.x, y-distance.y, z-distance.z)

		final override def equals(other : Any) = other match {
			case Point3f(ox,oy,oz) => x == ox && y == oy && z == oz
			//case Point3d(ox,oy,oz) =>
			case _ => false
		}

		//final override def asVector : Vec = Vector3f(x,y,z)
		//final override def asPoint3i : Point3i = Point3i(x.toInt,y.toInt,z.toInt)

		final def asVector3i = Vector3i(x.toInt,y.toInt,z.toInt)
		final def asVector3f = Vector3f(x,y,z)
		final def asVector3d = Vector3d(x,y,z)
    final override def asVector = asVector3f
		final def asPoint3i : Point3i = Point3i(x.toInt,y.toInt,z.toInt)
		final def asPoint3f : Point3f = this
		final def asPoint3d : Point3d = Point3d(x,y,z)
	}

	case class Point3d(val x : Double, val y : Double, val z : Double) extends PointD with Coordinates3[Double] {
		type Point = Point3d
		type Vec = Vector3d

		//final override def -(other : Point) : Vec = Vector3d(x-other.x, y-other.y, z-other.z)
		//final override def +(distance : Vec) : Point = Point3d(x+distance.x, y+distance.y, z+distance.z)
		//final override def -(distance : Vec) : Point = this + (-distance)

		final override def -(other : Point) : Vec = Vector3d(x-other.x, y-other.y, z-other.z)
		final override def +(distance : Vec) : Point = Point3d(x+distance.x, y+distance.y, z+distance.z)
		final override def -(distance : Vec) : Point = this + (-distance)

		final override def equals(other : Any) = other match {
			case Point3d(ox,oy,oz) => x == ox && y == oy && z == oz
			//case Point3f(ox,oy,oz) =>
			case _ => false
		}

		//final override def asVector : Vec = Vector3d(x,y,z)
		//final override def asVector : Vec = Vector3d(x,y,z)
		final def asVector3i = Vector3i(x.toInt,y.toInt,z.toInt)
		final def asVector3f = Vector3f(x,y,z)
		final def asVector3d = Vector3d(x,y,z)
    final override def asVector = asVector3d
		final def asPoint3i : Point3i = Point3i(x.toInt,y.toInt,z.toInt)
		final def asPoint3f : Point3f = Point3f(x.toFloat,y.toFloat,z.toFloat)
		final def asPoint3d : Point3d = this
	}

	case class Point3i(val x : Int, val y : Int, val z : Int) extends PointI with Coordinates3[Int] {
		type Point = Point3i
		type Vec = Vector3i

		final override def -(other : Point) : Vec = Vector3i(x-other.x, y-other.y, z-other.z)
		final override def +(distance : Vec) : Point = Point3i(x+distance.x.toInt, y+distance.y.toInt, z+distance.z.toInt)
		final override def -(distance : Vec) : Point = this + (-distance)
		//final override def -(distance : Vec) : Point = Point3f(x-distance.x, y-distance.y, z-distance.z)

		final override def equals(other : Any) = other match {
			case Point3i(ox,oy,oz) => x == ox && y == oy && z == oz
			//case Point3d(ox,oy,oz) =>
			case _ => false
		}

		//final override def asVector : Vec = Vector3f(x,y,z)
		//final override def asVector : Vec = Vector3i(x,y,z)
		final def asVector3i = Vector3i(x,y,z)
		final def asVector3f = Vector3f(x,y,z)
		final def asVector3d = Vector3d(x,y,z)
    final override def asVector = asVector3i
		final def asPoint3i : Point3i = this
		final def asPoint3f : Point3f = Point3f(x,y,z)
		final def asPoint3d : Point3d = Point3d(x,y,z)
		
		final def +(dimension : Dimension3) : Point = Point3i(x+dimension.width, y+dimension.y, z+dimension.z)
	}

}
