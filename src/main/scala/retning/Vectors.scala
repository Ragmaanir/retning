package retning

object Vectors {

	abstract class Vector[Scalar] {

		type Vec <: Vector[Scalar]
		type Point <: Points.GenericPoint[Scalar]

    def componentCount : Int
    def components : List[Scalar]
    def componentIndices = 0 to(componentCount-1)

		def apply(index : Int) : Scalar
    //def forall(condition : Scalar => Boolean) : Boolean
    //def forall(condition : Scalar => Boolean) = components.forall(condition)
    def forall(condition : Scalar => Boolean) = componentIndices.forall(i => condition(this(i)))

		def squaredLength : Scalar
		def length : Scalar
		def magnitude = length
		//def direction : Vector

		def unary_+() : Vec
		def unary_-() : Vec

		def *(s: Scalar) : Vec
		def /(s: Scalar) : Vec
		def \(s: Scalar) : Vec

		def +(s: Scalar) : Vec
		def -(s: Scalar) : Vec

		def +(other : Vec) : Vec
		def -(other : Vec) : Vec

		// TODO: scalarproduct & crossproduct
		//def *(other : Vec) : Scalar
		//def %(other : Vec) : Vec
		def multipliedBy(other : Vec) : Vec
		def /(other : Vec) : Vec
		def %(other : Vec) : Vec


		def abs : Vec
		def normalized : Vec
		def cross(other : Vec) : Vec
		def dot(other : Vec) : Scalar

    // FIXME: does this make sense? distance between two vectors?
		final def distanceTo(other : Vec) : Scalar = (this-other).length
		final def squaredDistanceTo(other : Vec) : Scalar = (this-other).squaredLength

		//final def isCloseTo(other : Vec, threshold : Scalar) = this.distanceTo(other) <= threshold
		def isCloseTo(other : Vec, threshold : Scalar) : Boolean
		def isCloseTo(other : Vec) : Boolean

		def isNormalized : Boolean

		/** Returns if for all components c_i: c_i >= other.c_i */
		def >>=(other : Vec) : Boolean
		/** Returns if for all components c_i: c_i <= other.c_i */
		def <<=(other : Vec) : Boolean

    def < (scalar : Scalar) : Boolean
    def > (scalar : Scalar) : Boolean

    def <= (scalar : Scalar) : Boolean
    def >= (scalar : Scalar) : Boolean

		def asPoint : Point
	}

	abstract class Vector3[Scalar](val x : Scalar, val y : Scalar, val z : Scalar) extends Vector[Scalar] {

    final override def componentCount = 3
    final override def components = List(x,y,z)

		//protected def instantiate(xx : Scalar, yy : Scalar, zz :Scalar) : Vec

		//final override def toString = "Vector3["+classOf[Scalar].toString+"]("+x+","+y+","+z+")"

		final override def apply(index : Int) = index match {
			case 0 => x
			case 1 => y
			case 2 => z
			case i => throw new IndexOutOfBoundsException("index out of range [0,2]: " + i)
		}
	}

	/**
	 * Float - Version of Vector3
	 * --------------------------
	 *
	 */
	object Vector3f {
		type Scalar = Float
		type Vec = Vector3f

		val DistanceThreshold = scala.Float.MinPositiveValue
    val EqualityThreshold = scala.Float.MinPositiveValue

		lazy val Zero = this(0)

		def apply(x : Double, y : Double, z : Double) : Vec = this(x.toFloat,y.toFloat,z.toFloat)
		def apply(s : Double) : Vec = this(s,s,s)

		def apply(x : Scalar, y : Scalar, z : Scalar) = new Vec(x,y,z)
		def apply(s : Scalar) : Vec = this(s,s,s)

		def apply(v : Vector3i) : Vec = this(v.x,v.y,v.z)
	}

	final class Vector3f private(x : Float, y : Float, z : Float) extends Vector3[Float](x,y,z) {

		import scala.math

		type Vec = Vector3f
		val Vec = Vector3f
		type Point = Points.Point3f
		type Scalar = Float

		final override def squaredLength = x*x + y*y + z*z
		final override def length = math.sqrt(squaredLength).toFloat

		final override def unary_+() : Vec = this
		final override def unary_-() = Vec(-x, -y, -z)

		final override def *(s: Scalar) = Vec(x * s, y * s, z * s)
		final override def /(s: Scalar) = { val inv = 1/s; Vec(x * inv, y * inv, z * inv) }
		final def /(s: Double) = { val inv = 1/s; Vec(x * inv, y * inv, z * inv) }
		final override def \(s: Scalar) = Vec(s / x, s / y, s / z)

		final override def +(s: Scalar) = Vec(x + s, y + s, z + s)
		final override def -(s: Scalar) = Vec(x - s, y - s, z - s)

		final override def +(o : Vec) = Vec(x+o.x, y+o.y, z+o.z)
		final override def -(o : Vec) = Vec(x-o.x, y-o.y, z-o.z)

		final override def multipliedBy(o : Vec) : Vec = Vec(x*o.x,y*o.y,z*o.z)
		final override def /(o : Vec) : Vec = Vec(x / o.x, y / o.y, z / o.z)
		final override def %(o : Vec) : Vec = Vec(x % o.x, y % o.y, z % o.z)

		final override def abs : Vec = Vec(math.abs(x),math.abs(y),math.abs(z))
		final override def normalized : Vec = { this / length }
		final override def cross(o : Vec) = Vec(y*o.z - z*o.y, z*o.x - x*o.z, x*o.y - y*o.x)
		final override def dot(o : Vec) : Scalar = x*o.x + y*o.y + z*o.z

		final override def isCloseTo(other : Vec, threshold : Scalar) : Boolean = this.distanceTo(other) < threshold
		final override def isCloseTo(other : Vec) : Boolean = this.isCloseTo(other,Vec.DistanceThreshold)

		final override def isNormalized : Boolean = squaredLength == 1

		final override def >>=(other : Vec) : Boolean = { x >= other.x && y >= other.y && z >= other.z }
		final override def <<=(other : Vec) : Boolean = { x <= other.x && y <= other.y && z <= other.z }

    final override def < (scalar : Scalar) : Boolean = forall(_ < scalar)
    final override def > (scalar : Scalar) : Boolean = forall(_ > scalar)

    final override def <= (scalar : Scalar) : Boolean = this < scalar || this == Vec(scalar)
    final override def >= (scalar : Scalar) : Boolean = this > scalar || this == Vec(scalar)

		final override def toString = "Vector3f("+x+","+y+","+z+")"

    final def isApproximatelyEqualTo(other : Vec, threshold : Scalar = Vec.EqualityThreshold) : Boolean = (this - other) <= threshold

		override def hashCode = (x+y+z).toInt
		override def equals(other : Any) = other match {
			//case v : Vec => x == v.y && y == v.y && z == v.z
      case v : Vec => isApproximatelyEqualTo(v)
			case _ => false
		}

		/** Dimension interoperability */
		def *(dim : Dimensions.Dimension3) : Vec = Vec(x * dim.x, y*dim.y, z*dim.z)

		def asPoint : Point = Points.Point3f(x,y,z)
	}

	/**
	 * Double - Version of Vector3
	 * ---------------------------
	 *
	 */
	object Vector3d {
		type Scalar = Double
		type Vec = Vector3d

    val DistanceThreshold = scala.Double.MinPositiveValue
    val EqualityThreshold = scala.Double.MinPositiveValue

		lazy val Zero = this(0)

		def apply(x : Scalar, y : Scalar, z : Scalar) = new Vec(x,y,z)
		def apply(s : Scalar) : Vec = this(s,s,s)

		def apply(v : Vector3i) : Vec = this(v.x,v.y,v.z)
	}

	final class Vector3d private(x : Double, y : Double, z : Double) extends Vector3[Double](x,y,z) {

		import scala.math

		type Vec = Vector3d
		val Vec = Vector3d
		type Point = Points.Point3d
		type Scalar = Double

		final override def squaredLength = x*x + y*y + z*z
		final override def length = math.sqrt(squaredLength)

		final override def unary_+() : Vec = this
		final override def unary_-() = Vec(-x, -y, -z)

		final override def *(s: Scalar) = Vec(x * s, y * s, z * s)
		final override def /(s: Scalar) = { val inv = 1/s; Vec(x * inv, y * inv, z * inv) }
		final override def \(s: Scalar) = Vec(s / x, s / y, s / z)

		final override def +(s: Scalar) = Vec(x + s, y + s, z + s)
		final override def -(s: Scalar) = Vec(x - s, y - s, z - s)

		final override def +(o : Vec) = Vec(x+o.x, y+o.y, z+o.z)
		final override def -(o : Vec) = Vec(x-o.x, y-o.y, z-o.z)

		final override def multipliedBy(o : Vec) : Vec = Vec(x*o.x,y*o.y,z*o.z)
		final override def /(o : Vec) : Vec = Vec(x / o.x, y / o.y, z / o.z)
		final override def %(o : Vec) : Vec = Vec(x % o.x, y % o.y, z % o.z)

		final override def abs : Vec = Vec(math.abs(x),math.abs(y),math.abs(z))
		final override def normalized : Vec = { this / length }
		final override def cross(o : Vec) = Vec(y*o.z - z*o.y, z*o.x - x*o.z, x*o.y - y*o.x)
		final override def dot(o : Vec) : Scalar = x*o.x + y*o.y + z*o.z

		final override def isCloseTo(other : Vec, threshold : Scalar) : Boolean = this.distanceTo(other) < threshold
		final override def isCloseTo(other : Vec) : Boolean = this.isCloseTo(other,Vec.DistanceThreshold)

		final override def isNormalized : Boolean = squaredLength == 1

		final override def >>=(other : Vec) : Boolean = { x >= other.x && y >= other.y && z >= other.z }
		final override def <<=(other : Vec) : Boolean = { x <= other.x && y <= other.y && z <= other.z }

    final override def < (scalar : Scalar) : Boolean = forall(_ < scalar)
    final override def > (scalar : Scalar) : Boolean = forall(_ > scalar)

    final override def <= (scalar : Scalar) : Boolean = this < scalar || this == Vec(scalar)
    final override def >= (scalar : Scalar) : Boolean = this > scalar || this == Vec(scalar)

		final override def toString = "Vector3d("+x+","+y+","+z+")"

    final def isApproximatelyEqualTo(other : Vec, threshold : Scalar = Vec.EqualityThreshold) : Boolean = (this - other) <= threshold

		override def hashCode = (x+y+z).toInt
		override def equals(other : Any) = other match {
			//case v : Vec => x == v.y && y == v.y && z == v.z
      case v : Vec => isApproximatelyEqualTo(v)
			case _ => false
		}

		/** Dimension interoperability */
		def *(dim : Dimensions.Dimension3) : Vec = Vec(x * dim.x, y*dim.y, z*dim.z)

		def asPoint : Point = Points.Point3d(x,y,z)
	}

	/**
	 * Integer - Version of Vector3
	 * ----------------------------
	 *
	 */
	object Vector3i {
		type Scalar = Int
		type Vec = Vector3i
		val Vec = Vector3i

		val DistanceThreshold = 0

		lazy val Zero = Vec(0)

		def apply(x : Scalar, y : Scalar, z : Scalar) = new Vec(x,y,z)
		def apply(s : Scalar) : Vec = Vec(s,s,s)
	}

	final class Vector3i private(x : Int, y : Int, z : Int) extends Vector3[Int](x,y,z) {

		import scala.math

		type Vec = Vector3i
		val Vec = Vector3i
		type Scalar = Int
		type Point = Points.Point3i

		final override def squaredLength = x*x + y*y + z*z
		final override def length = math.sqrt(squaredLength).toInt

		final override def unary_+() : Vec = this
		final override def unary_-() = Vec(-x, -y, -z)

		final override def *(s: Scalar) = Vec(x * s, y * s, z * s)
		final override def /(s: Scalar) = { val inv = 1/s; Vec(x * inv, y * inv, z * inv) }
		final override def \(s: Scalar) = Vec(s / x, s / y, s / z)

		final override def +(s: Scalar) = Vec(x + s, y + s, z + s)
		final override def -(s: Scalar) = Vec(x - s, y - s, z - s)

		final override def +(o : Vec) = Vec(x+o.x, y+o.y, z+o.z)
		final override def -(o : Vec) = Vec(x-o.x, y-o.y, z-o.z)

		final override def multipliedBy(o : Vec) : Vec = Vec(x*o.x,y*o.y,z*o.z)
		final override def /(o : Vec) : Vec = Vec(x / o.x, y / o.y, z / o.z)
		final override def %(o : Vec) : Vec = Vec(x % o.x, y % o.y, z % o.z)

		final override def abs : Vec = Vec(math.abs(x),math.abs(y),math.abs(z))
		final override def normalized : Vec = { this / length }
		final override def cross(o : Vec) = Vec(y*o.z - z*o.y, z*o.x - x*o.z, x*o.y - y*o.x)
		final override def dot(o : Vec) : Scalar = x*o.x + y*o.y + z*o.z

		final override def isCloseTo(other : Vec, threshold : Scalar) : Boolean = this.distanceTo(other) < threshold
		final override def isCloseTo(other : Vec) : Boolean = this.isCloseTo(other,Vec.DistanceThreshold)

		final override def isNormalized : Boolean = squaredLength == 1

		final override def >>=(other : Vec) : Boolean = { x >= other.x && y >= other.y && z >= other.z }
		final override def <<=(other : Vec) : Boolean = { x <= other.x && y <= other.y && z <= other.z }

    final override def < (scalar : Scalar) : Boolean = forall(_ < scalar)
    final override def > (scalar : Scalar) : Boolean = forall(_ > scalar)

    final override def <= (scalar : Scalar) : Boolean = this < scalar || this == Vec(scalar)
    final override def >= (scalar : Scalar) : Boolean = this > scalar || this == Vec(scalar)

		final override def toString = "Vector3i("+x+","+y+","+z+")"

		override def hashCode = (x+y+z).toInt
		override def equals(other : Any) = other match {
			case v : Vec => x == v.y && y == v.y && z == v.z
			case _ => false
		}

		def asPoint : Point = Points.Point3i(x,y,z)
	}


	/*
	abstract class Vector[Scalar, Vec <: Vector[Scalar,Vec]] {

		def apply(index : Int) : Scalar

		def squaredLength : Scalar
		def length : Scalar

		def unary_+() : Vec
		def unary_-() : Vec

		def *(s: Scalar) : Vec
		def /(s: Scalar) : Vec
		def \(s: Scalar) : Vec

		def +(s: Scalar) : Vec
		def -(s: Scalar) : Vec

		def +(other : Vec) : Vec
		def -(other : Vec) : Vec

		def abs : Vec
		def normalized : Vec
		def cross(other : Vec) : Vec
		def dot(other : Vec) : Scalar

		final def distanceTo(other : Vec) : Scalar = (this-other).length
		final def squaredDistanceTo(other : Vec) : Scalar = (this-other).squaredLength

		//final def isCloseTo(other : Vec, threshold : Scalar) = this.distanceTo(other) <= threshold
		def isCloseTo(other : Vec, threshold : Scalar) : Boolean
		def isCloseTo(other : Vec) : Boolean
	}

	abstract class Vector3[Scalar,Vec <: Vector3[Scalar,Vec]](val x : Scalar, val y : Scalar, val z : Scalar) extends Vector[Scalar,Vec] {

		//protected def instantiate(xx : Scalar, yy : Scalar, zz :Scalar) : Vec

		//final override def toString = "Vector3["+classOf[Scalar].toString+"]("+x+","+y+","+z+")"

		final override def apply(index : Int) = index match {
			case 0 => x
			case 1 => y
			case 2 => z
			case i => throw new IndexOutOfBoundsException("index out of range [0,2]: " + i)
		}
	}

	object Vector3f {
		val DistanceThreshold = scala.Float.MinPositiveValue

		val Zero = Vector3f(0,0,0)

		def apply(x : Double, y : Double, z : Double) = new Vector3f(x.toFloat,y.toFloat,z.toFloat)
		def apply(s : Double) : Vector3f = Vector3f(s,s,s)

		def apply(x : Float, y : Float, z : Float) = new Vector3f(x,y,z)
		def apply(s : Float) : Vector3f = Vector3f(s,s,s)
	}

	final class Vector3f(x : Float,y : Float,z : Float) extends Vector3[Float,Vector3f](x,y,z) {

		import scala.math

		type Scalar = Float

		final override def squaredLength = x*x + y*y + z*z
		final override def length = math.sqrt(squaredLength).toFloat

		final override def unary_+() : Vector3f = this
		final override def unary_-() = Vector3f(-x, -y, -z)

		final override def *(s: Scalar) = Vector3f(x * s, y * s, z * s)
		final override def /(s: Scalar) = { val inv = 1/s; Vector3f(x * inv, y * inv, z * inv) }
		final override def \(s: Scalar) = Vector3f(s / x, s / y, s / z)

		final override def +(s: Scalar) = Vector3f(x + s, y + s, z + s)
		final override def -(s: Scalar) = Vector3f(x - s, y - s, z - s)

		final override def +(o : Vector3f) = Vector3f(x+o.x, y+o.y, z+o.z)
		final override def -(o : Vector3f) = Vector3f(x-o.x, y-o.y, z-o.z)

		final override def abs : Vector3f = Vector3f(math.abs(x),math.abs(y),math.abs(z))
		final override def normalized : Vector3f = { this / length }
		final override def cross(o : Vector3f) = Vector3f(y*o.z - z*o.y, z*o.x - x*o.z, x*o.y - y*o.x)
		final override def dot(o : Vector3f) : Scalar = x*o.x + y*o.y + z*o.z

		final override def isCloseTo(other : Vector3f, threshold : Scalar) : Boolean = this.distanceTo(other) < threshold
		final override def isCloseTo(other : Vector3f) : Boolean = this.isCloseTo(other,Vector3f.DistanceThreshold)

		final override def toString = "Vector3f("+x+","+y+","+z+")"

		override def hashCode = (x+y+z).toInt
		override def equals(other : Any) = other match {
			case v : Vector3f => x == v.y && y == v.y && z == v.z
			case _ => false
		}
	}
	*/

}
