package retning


object Dimensions {

	sealed trait Dimension {
		def count : Int

		def apply(i : Int) : Int
		def sizes : List[Int] = 0.to(count-1).map(this(_)).toList
		def area : Int = sizes.product
	}

	case class Dimension2(val width : Int, val height : Int) extends Dimension {
		final def count = 2

		final def x = width
		final def y = height

		final def apply(i : Int) = i match {
			case 0 => width
			case 1 => height
			case _ => sys.error("Invalid dimension index: "+i)
		}
	}

	case class Dimension3(val width : Int, val height : Int, val depth : Int) extends Dimension {
		final def count = 3

		final def x = width
		final def y = height
		final def z = depth

		final def apply(i : Int) = i match {
			case 0 => width
			case 1 => height
			case 2 => depth
			case _ => sys.error("Invalid dimension index: "+i)
		}

		final def asVector = Vectors.Vector3f(width,height,depth)
	}

}
