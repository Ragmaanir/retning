package retning

object Volumes {

	import Vectors._

	type Point3r = Points.Point3f
	val Point3r = Points.Point3f
	type Scalar = Float

	/*
	def intersection(volume : Volumetric, other : Volumetric) : Option[Intersection] = {

	}
	*/

	/**
	 * Mixes in properties so an object behaves volume-like.
	 */
	trait Volumetric {
		def minPoint : Point3r
		def maxPoint : Point3r
		def midPoint : Point3r = minPoint + (maxPoint - minPoint)/2.0

		/**
		 * True iff the (solid) volumes intersect. The second parameter is used internally
		 * to prevent infinite recursions.
		 */
		def intersectsVolume(other : Volumetric, reversed : Boolean = false) : Boolean = reversed match {
			case true => sys.error("intersection test between "+this.getClass+" and "+other.getClass+" is not implemented")
			case false => other.intersectsVolume(this,true)
		}

		//def containsVolume(other : Volume) : Boolean
		def containsPoint(point : Point3r) : Boolean
	}

	trait Volume extends Volumetric {

	}

	trait Spherical extends Volumetric {
		def centerPoint : Point3r
		def radius : Scalar

		def minPoint = centerPoint - Vector3f(radius)
		def maxPoint = centerPoint + Vector3f(radius)

		override def intersectsVolume(other : Volumetric, reversed : Boolean = false) : Boolean = {
			require(other!=null)
			other match {
				case spherical : Spherical => {
					val dist = centerPoint - spherical.centerPoint
					dist.length <= radius + spherical.radius
				}
				case cuboidal : Cuboidal => {
					var dist = 0.0d

					/*
					if(centerPoint.x < cuboidic.minPoint.x) {
						dist += math.pow(centerPoint.x - cuboidic.minPoint.x,2)
					} else if(centerPoint.x > cuboidic.maxPoint.x) {
						dist += math.pow(centerPoint.x - cuboidic.maxPoint.x,2)
					}
					*/
					
					for(ci <- 0.to(2)) {
						if(centerPoint(ci) < cuboidal.minPoint(ci)) {
							dist += math.pow(centerPoint(ci) - cuboidal.minPoint(ci),2)
						} else if(centerPoint(ci) > cuboidal.maxPoint(ci)) {
							dist += math.pow(centerPoint(ci) - cuboidal.maxPoint(ci),2)
						}
					}

					dist < radius*radius
				}
				/*case _ => reversed match {
					case true => sys.error("intersection test between "+this.getClass+" and "+other.getClass+" is not implemented")
					case false => other.intersectsVolume(this,true)
				}*/
				case _ => super.intersectsVolume(other,reversed)
			}
		}
		def containsPoint(point : Point3r) = (centerPoint-point).length <= radius
	}

	case class Sphere(val centerPoint : Point3r, val radius : Scalar) extends Volume with Spherical {
		//def minPoint = centerPoint - Vector3f(radius)
		//def maxPoint = centerPoint + Vector3f(radius)
	}

  /* Axis-Aligned, rectangular and cuboidal */
	trait Cuboidal extends Volumetric {
		def minPoint : Point3r
		def maxPoint : Point3r
		require(minPoint <<= maxPoint)

		def size = maxPoint-minPoint
		def centerPoint : Point3r = minPoint + size/2
		//def centerPoint = minPoint + size/2
		def area = size.x * size.y * size.z

		def containsPoint(point : Point3r) = (minPoint <<= point) && (maxPoint >>= point)
	}

  object Cuboid {
    def apply(centerPoint : Point3r, size : Vector3f) : Cuboid = Cuboid(centerPoint - size/2, centerPoint + size/2)
  }

	// TODO: rename? because this is an axis aligned cuboid!
	case class Cuboid(val minPoint : Point3r, val maxPoint : Point3r) extends Volume with Cuboidal
}
